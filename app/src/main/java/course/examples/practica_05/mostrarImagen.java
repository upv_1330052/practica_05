package course.examples.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class mostrarImagen extends AppCompatActivity {

    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_imagen);


        // Obten el mensaje del intent
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        int num=Integer.parseInt(message);
        image = (ImageView) findViewById(R.id.imageView1);

        if(num == 1){
            image.setImageResource(R.drawable.pina); //nombre del archivo imagen
        }else if(num==2){
            image.setImageResource(R.drawable.sandia); //nombre del archivo imagen
        }else if(num==3){
            image.setImageResource(R.drawable.uvas); //nombre del archivo imagen
        }else{
            image.setImageResource(R.drawable.manzana); //nombre del archivo imagen
        }
    }
}
